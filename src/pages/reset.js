import React, { useState, useEffect } from 'react'
import { ActionButton, FormInput, Layout } from '../components';
import { useFirebase } from '../Hooks';
import { useHistory } from 'react-router-dom'

export const Reset = () => {
    const [pass, setPass] = useState('');
    const { auth } = useFirebase();
    const [verify, setVerify] = useState('')
    const history = useHistory()
    let oobCode = history.location.search.split('&');
    oobCode = oobCode[1].split('oobCode=')[1]
    console.log(oobCode)
    console.log(history)
    const update = () => {
        auth.confirmPasswordReset(oobCode, pass ).then(() => {
            // Email sent.
            console.log("Updated")
        }).catch(function (error) {
            console.log(error)
        });
    }
    console.log(auth)

    useEffect(() => {
        console.log(auth)
        if (auth && oobCode) {
            console.log('UTGATAI BOL')
            auth.verifyPasswordResetCode(oobCode)
                .then((email) => {
                    setVerify('verified')
                    console.log(email)
                })
                .catch((error) => {
                    console.log(error)
                })
        }
    }, [auth, oobCode])


    // console.log(verify)

    return (
        <Layout>
            <div className='flex justify-center'>
                {
                    (verify === 'verified') &&
                    <div className='h-vh-70'>
                        <h1 className='fs-24 title'>Нууц үг солих</h1>
                        <FormInput value={pass} type='password' onChange={(e) => setPass(e.target.value)} placeholder='Шинэ нууц үг' className='mb-8 pl-44'></FormInput>
                        <FormInput value={pass} type='password' onChange={(e) => setPass(e.target.value)} placeholder='Давтаж оруулна уу' className='mb-8 pl-44'></FormInput>
                        <ActionButton onClick={update} className='margin-auto'>Шинэ нууц үг</ActionButton>
                    </div>
                }
            </div>
        </Layout>
    )
}