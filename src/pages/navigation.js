import React, { useState } from 'react'
import { DropdownMenu, ArrowIcon, Grid } from '../components/';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';

export const Navigation = ({ children }) => {
    
    let history = useHistory();
    let [checked, setChecked] = useState('home');

    return (
        <Grid className='h-70 b-white w100' columns="3">
            <div className='flex items-center justify-start b-white'>
                {
                    !_.isEmpty(
                        _.chain([
                            '/',
                        ])
                            .filter(path => path === '/' ? history.location.pathname === '/' : history.location.pathname.match(path))
                            .value()
                    )
                    && <h3 className="font-glegoo ml-10 fs-24 lh-43" onClick={() => { history.push("/") }}>Chingis</h3>
                }

                {
                    !_.isEmpty(
                        _.chain([
                            '/profile',
                        ])
                            .filter(path => path === '/' ? history.location.pathname === '/' : history.location.pathname.match(path))
                            .value()
                    )
                    && <div className='items-center flex'>
                        <ArrowIcon onClick={() => { history.goBack() }} width={20} height={20} />
                        <span className='fs-20 ml-10'>Profile</span>
                    </div>
                }

                {
                    !_.isEmpty(
                        _.chain([
                            '/vote-history',
                        ])
                            .filter(path => path === '/' ? history.location.pathname === '/' : history.location.pathname.match(path))
                            .value()
                    )
                    && <div className='items-center flex'>
                        <ArrowIcon onClick={() => { history.goBack() }} width={20} height={20} />
                        <span className='fs-20 ml-10'>History</span>
                    </div>
                }

                {
                    !_.isEmpty(
                        _.chain([
                            '/event-id',
                            '/search',
                            '/tender-id',
                            "/verify-account"
                        ])
                            .filter(path => path === '/' ? history.location.pathname === '/' : history.location.pathname.match(path))
                            .value()
                    )
                    && <ArrowIcon onClick={() => { history.goBack() }} width={20} height={20} />
                }

            </div>
            <div className='flex items-center justify-center'>
            </div>
            <div className='flex items-center justify-end'>
                {
                    !_.isEmpty(
                        _.chain([
                            '/',
                            '/event-id',
                            '/search',
                            '/tender-id',
                            '/profile'
                        ])
                            .filter(path => path === '/' ? history.location.pathname === '/' : history.location.pathname.match(path))
                            .value()
                    )
                    && <DropdownMenu ></DropdownMenu>
                }

                {
                    !_.isEmpty(
                        _.chain([
                            '/register'
                        ])
                            .filter(path => path === '/' ? history.location.pathname === '/' : history.location.pathname.match(path))
                            .value()
                    )
                    && <p>Algasah</p>
                }

            </div>
        </Grid>
    )
}