import React from 'react'

export const VerifiedIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 70 70" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="35" cy="35" r="35" fill="#1DD3D2" />
                <path d="M35.0003 20.4166L39.5066 30.0162L49.5837 31.565L42.292 39.033L44.0128 49.5833L35.0003 44.5995L25.9878 49.5833L27.7087 39.033L20.417 31.565L30.4941 30.0162L35.0003 20.4166Z" fill="white" stroke="white" stroke-width="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}