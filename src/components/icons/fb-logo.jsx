import React from 'react'

export const FbIcon = ({height, width, color = "#FFFFF", ...others}) => {

    return (
        <span {...others}>
           <svg width={width} height={height} viewBox="0 0 13 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M12.1783 0.987061H9.15982C7.82558 0.987061 6.54599 1.51384 5.60253 2.45153C4.65908 3.38921 4.12906 4.66098 4.12906 5.98706V8.98706H1.1106V12.9871H4.12906V20.9871H8.15367V12.9871H11.1721L12.1783 8.98706H8.15367V5.98706C8.15367 5.72184 8.25968 5.46749 8.44837 5.27995C8.63706 5.09242 8.89298 4.98706 9.15982 4.98706H12.1783V0.987061Z" stroke="white" strokeLinecap="round" strokeLinejoin="round"/>
            </svg>
        </span>
    )
}