import React from 'react';

export const Box = (props) => {
    let { children, type = "default", disabled, className, ...others } = props;

    const getBorderClass = () => type === "top" ? "box-top" : type === "bottom" ? "box-bottom" : "box";
   
    return (
        <div className={`b-white ${getBorderClass()} ${className}`} {...others}>
            <div className='container'>
                {/* ADD A pv-${i}(PADDING TOP & BOTTOM) URSELF */}
                {children}
            </div>
        </div>
    );
};