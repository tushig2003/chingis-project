import React, { useState, useRef } from 'react'
import { Stack, MenuIcon, ProfileIcon, NewProjectIcon, XIcon, HomeIcon, LogOut } from '../components'
import { DownArrow } from './icons';
import { useHistory } from 'react-router-dom'
import { useOutsideClick } from '../Hooks/use-outside-click'
import { useFirebase } from '../Hooks/firebase'

export const DropdownItem = ({ children, onClick, ...others }) => {
    return (
        <div className='flex-row items-center' onClick={onClick} {...others}>
            {children}
        </div>
    )
}

export const DropdownItemArrow = ({ children, onClick, ...others }) => {
    return (
        <div className='flex-center flex-row' onClick={onClick} {...others}>
            {children}
        </div>
    )
}

export const DropdownOptions = ({ children }) => {
    return (
        <Stack className='flex-row absolute t-1 r-0 l-0 b-default w95 bshadow h-200 z-i bradius-10' size={4}>
            {children}
        </Stack>
    )
}

export const DropdownOptionsArrow = ({ children }) => {
    return (
        <Stack className='text-center font-DmSans b-default br-inactive-1 bradius-10' size={2}>
            {children}
        </Stack>
    )
}


export const DropdownMenu = () => {
    const history = useHistory();
    const [show, setShow] = useState(false);
    const ref = useRef();
    const { auth } = useFirebase();

    useOutsideClick(ref, () => { setShow(false) });

    return (
        <div className='z-i bradius-10'>
            <div className='container flex-row justify-evenly'>
                {!show && <MenuIcon height={12} width={18} onClick={() => setShow(true)} className="mr-15" />
                }
                {show &&
                    <DropdownOptions>
                        <XIcon className='absolute pa-5' height={12} width={18} onClick={() => setShow(false)} />
                        <DropdownItem onClick={() => { history.push('/addEvent') }}>
                            <NewProjectIcon height={18} width={18} />
                            <div className='ml-10 font-DmSans'>New Project</div>
                        </DropdownItem>
                        <DropdownItem onClick={() => { history.push('/profile') }}>
                            <ProfileIcon height={16} width={14} />
                            <div className='ml-10 font-DmSans'>Profile</div>
                        </DropdownItem>
                        <DropdownItem onClick={() => { history.push('/') }}>
                            <HomeIcon height={19} width={16} />
                            <div className='ml-10 font-DmSans'>Home</div>
                        </DropdownItem>
                        <DropdownItem onClick={() => { auth.signOut() }}>
                            <LogOut height={19} width={16} />
                            <div className='ml-10 font-DmSans c-warning'>Log Out</div>
                        </DropdownItem>
                    </DropdownOptions>}
            </div>
        </div>
    )
}

export const DropdownMenuArrow = ({ type, setType }) => {
    const [show, setShow] = useState(false);
    const ref = useRef()

    useOutsideClick(ref, () => { setShow(false) });

    return (
        <div className='pr font-main w-150 display-block mr-15' ref={ref}>
            {!show && <div className="fs-16 lh-21 text-right mt-12 flex bold justify-end items-center w100" onClick={() => { setShow((show) => !show) }}>
                {type}
                <DownArrow className="ml-10 mt-6 mr-5" height={18} width={12} />
            </div>}
            {show &&
                <div className='pr font-main w-150 display-block' ref={ref}>
                    <div className="fs-16 lh-21 text-right mt-12 flex bold justify-end items-center w100" onClick={() => { setShow((show) => !show) }}>
                        <div className='flex items-center'>
                            <div className='fs-16 lh-21 text-right display-block justify-end items-center'>Sort By {type}</div>
                            <DownArrow className="ml-10 mt-6 mr-5" height={18} width={12} />
                        </div>
                    </div>
                    <div className='absolute w100'>
                        <DropdownOptionsArrow>
                            <DropdownItemArrow onClick={() => setType('New')}>
                                <div className='mt-15 mb-5'>New</div>
                            </DropdownItemArrow>
                            <div className="b-inactive w100 h-1" />
                            <DropdownItemArrow onClick={() => setType('Popular')}>
                                <div className='mb-15 mt-5'>Popular</div>
                            </DropdownItemArrow>
                        </DropdownOptionsArrow>
                    </div>
                </div>}
        </div>
    )
}