import React from 'react';

export const ProgressBar = ({ vote, voteCount }) => {
    return (
        <div className='pr mt-3'>
            <div className='flex justify-center'>
                <div className='w90 absolute bradius-10 b-gray4 h-10 mt-7' />
            </div>
            <div className='absolute bradius-50 b-primary h-10 ml-17' style={{ width: `${(vote / voteCount) * 100 || 1}%` }} />
        </div>
    )
}