import React from 'react';
import { Navigation } from '../pages/navigation';

export const Layout = ({ children }) => {

    return (
        <div className='containerpf b-gray6 items-center'>
            <Navigation />
            {children}
        </div>
    )
}