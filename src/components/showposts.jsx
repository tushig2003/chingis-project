import React, { useState } from 'react';
import { EventRender, Stack } from '../components'
import { SortBy } from './sortby';

export const ShowPosts = ({ data }) => {

    const [sort, setSort] = useState('New');

    return (
        <div className="container-bruh">
            <div className="flex-row justify-between items-center w100">
                <div className="font-main fs-24 lh-31 bold mt-15 ml-20">Posts</div>
                <SortBy type={sort} setType={setSort} />
            </div>
            <div className='flex-col mt-16 mb-16'>
                {sort === 'New' && data.sort((a, b) => a.createdAt > b.createdAt ? -1 : 1).map((e) => <EventRender key={e.id} {...e} />)}
                {sort === 'Popular' && data.sort((a, b) => a.vote > b.vote ? -1 : 1).map((e) => <EventRender key={e.id} {...e} />)}
            </div>
        </div>
    )
}