import React from 'react';
import { SignUp } from './pages/sign-up';
import { SignIn } from './pages/sign-in';
import { HomeDefault } from './pages/home-default';
import { AddEvent } from './pages/add-event'
import { Verify } from './pages/verify-account';
import { EventSharing } from './pages/event-sharing';
import { TenderSharing } from './pages/tender-sharing';
// import { Navigation } from './pages/navigation';
import { Components } from './pages/components';
import { Reset } from './pages/reset'
import Profile from './pages/profile'
import { ForgotPass } from './pages/forgot-password'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { AuthUserProvider } from './providers/auth-user-provider';
import './style/grid.scss'
import './style/grid-align.scss'
import './style/main.scss';
import { VoteHistory } from './pages/vote-history';

const App = () => {
    return (
        <AuthUserProvider>
            <Router>
                <Switch>
                    <Route path="/login">
                        <SignIn />
                    </Route>
                    <Route path="/register">
                        <SignUp />
                    </Route>
                    <Route path="/addEvent">
                        <AddEvent />
                    </Route>
                    <Route path="/event-id">
                        <EventSharing />
                    </Route>
                    <Route path="/tender-id">
                        <TenderSharing />
                    </Route>
                    <Route path="/verify-account">
                        <Verify />
                    </Route>
                    <Route path="/profile">
                        <Profile />
                    </Route>
                    <Route path="/vote-history">
                        <VoteHistory />
                    </Route>
                    <Route path="/reset">
                        <Reset />
                    </Route>
                    <Route path="/forgot-pass">
                        <ForgotPass />
                    </Route>
                    <Route path="/components">
                        <Components />
                    </Route>
                    <Route path="/">
                        <HomeDefault />
                    </Route>
                </Switch>
            </Router>
        </AuthUserProvider>
    )
}

export default App
